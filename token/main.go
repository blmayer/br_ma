package token

import (
	"br_ma/mysql"
	"br_ma/user"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const uri = "https://oauth2.googleapis.com/tokeninfo?id_token="

// GetEmail verifies a token on the google endpoint
func GetEmail(token string) string {
	res, err := http.Get(uri + url.QueryEscape(token[7:]))
	if err != nil {
		log.Println(err)
		return ""
	}
	defer res.Body.Close()

	var data map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		log.Println(err)
		return ""
	}
	email, ok := data["email"]
	if !ok {
		return ""
	}
	return email.(string)
}

// GetUser verifies the token
func GetUser(auth string) (u user.User, err error) {
	if len(auth) < 6 {
		return u, fmt.Errorf("Invalid token")
	}

	if len(auth) > 6 && auth[:5] == "Basic" {
		u, err = getFromToken(auth)
		if err != nil {
			return
		}
	} else {
		// Check token with googleapi
		email := GetEmail(auth)
		if email == "" {
			return u, fmt.Errorf("")
		}
		u = user.Get(email)
		if u.Email == "" {
			return u, fmt.Errorf("User not found, please sign up first.")
		}
	}

	return
}

func getFromToken(token string) (u user.User, err error) {
	// Open token and search
	info, err := base64.URLEncoding.DecodeString(token[6:])
	if err != nil {
		return
	}
	data := strings.Split(string(info), ":")

	// Make query on mysql
	mysql.DB.QueryRow(`
	SELECT
		email,
		password,
		fullName,
		telephone,
		address
	FROM test.users
	WHERE email = ?`,
		data[0],
	).Scan(&u.Email, &u.Password, &u.Name, &u.Telephone, &u.Address)

	if u.Email == "" {
		err = fmt.Errorf("User not found, please sign up first.")
		return
	}

	if u.Password != data[1] {
		if u.Password == "" {
			err = fmt.Errorf("You registered with Google.")
		} else {
			err = fmt.Errorf("Wrong password.")
		}
		return
	}

	return
}
