package mysql

import (
	"database/sql"

	// This is just a driver
	_ "github.com/go-sql-driver/mysql"
)

// DB is the connection to the production database
var DB *sql.DB

// NullString is the nullable string type
type NullString struct {
	sql.NullString
}

// NullBool is the nullable bool type
type NullBool struct {
	sql.NullBool
}

func init() {
	// Open up our database connection
	var err error
	DB, err = sql.Open("mysql", "staff:BR_MA@tcp(34.71.94.245)/test")
	if err != nil {
		panic(err.Error())
	}

	// If there is an error opening the connection, handle it
	err = DB.Ping()
	if err != nil {
		panic(err.Error())
	}
}
