package mysql

import (
	"testing"
)

func TestQuery(t *testing.T) {
	defer DB.Close()

	var result []string
	rows, err := DB.Query("select email from test.users limit 10")
	if err != nil {
		t.Errorf("Query returned error: " + err.Error())
		return
	}
	defer rows.Close()

	for rows.Next() {
		var email string
		err = rows.Scan(&email)
		if err != nil {
			t.Errorf("Scan returned error: " + err.Error())
			return
		}

		result = append(result, email)
	}

	if len(result) == 0 {
		t.Error("Received 0 rows")
	}
}
