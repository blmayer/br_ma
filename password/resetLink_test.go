package password

import "testing"

func TestResetLink(t *testing.T) {
	err := SendResetLink("bleemayer@gmail.com", 100000)
	if err != nil {
		t.Error(err.Error())
	}
}
