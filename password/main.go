package password

import (
	"br_ma/mysql"
	"fmt"
	"net/smtp"
	"time"
)

const (
	mailServer = "smtp.gmail.com"
	mailPort   = ":465"
	mailUser   = "bleemayer@gmail.com"
	mailPass   = "dvqaplvbqxvdsseu"
)

// SetTemp sets an expiration field
func SetTemp(email string) (int64, error) {
	// Check email existence
	var check string
	err := mysql.DB.QueryRow(`
	SELECT
		email
	FROM test.users
	WHERE email = ?
	AND token = "internal"`,
		email,
	).Scan(&check)
	if err != nil {
		return 0, fmt.Errorf("User not found, please sign up first.")
	}

	expiration := time.Now().Add(15 * time.Minute).UnixNano()

	_, err = mysql.DB.Exec(`
	UPDATE
	test.users
	SET linkExpiration = ?
	WHERE email = ?`,
		expiration, email,
	)
	return expiration, err
}

// SendResetLink sends a password reset link to a user
func SendResetLink(email string, expiration int64) error {
	msg := []byte(
		"From: bleemayer@gmail.com\r\n" +
			"To: " + email + "\r\n" +
			"Subject: Password reset link\r\n" +
			"\r\n" +
			"Here’s your password reset link:\r\n\r\n" +
			fmt.Sprintf("https://blmayer.herokuapp.com/resetpass.html?id=%d\r\n\r\n", expiration) +
			"Thanks for using br_ma!\r\n\r\n" +
			"br_ma team.",
	)

	err := smtp.SendMail(
		"smtp.gmail.com:587",
		smtp.PlainAuth("", "bleemayer@gmail.com", mailPass, "smtp.gmail.com"),
		"bleemayer@gmail.com", []string{email}, []byte(msg),
	)

	if err != nil {
		return err
	}
	return nil
}

// CheckExpiration checks if a resetpass link is valid
func CheckExpiration(expiration int64) error {
	var email string
	err := mysql.DB.QueryRow(`
	SELECT
		email
	FROM test.users
	WHERE linkExpiration = ?`,
		expiration,
	).Scan(&email)

	if err != nil {
		return err
	}

	if email == "" {
		return fmt.Errorf("Link not found")
	}

	if time.Now().UnixNano() > expiration {
		return fmt.Errorf("Link expired")
	}

	return nil
}

// Reset inserts the new password and removes the expiration
func Reset(expiration int64, newPass string) error {
	// // Get email
	// var email string
	// err := mysql.DB.QueryRow(
	// 	"SELECT email FROM test.users WHERE expiration = ?",
	// 	expiration,
	// ).Scan(&email)
	// if err != nil {
	// 	return err
	// }

	// newHash := base64.URLEncoding.EncodeToString([]byte(email + ":" + newPass))
	_, err := mysql.DB.Exec(`
	UPDATE
	test.users
	SET password = ?
	WHERE linkExpiration = ?`,
		newPass,
		expiration,
	)

	return err
}
