package handlers

import (
	"br_ma/password"
	"br_ma/token"
	"br_ma/user"
	"encoding/base64"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"text/template"
)

// PutHandler is used to handle updates comming from the website
func PutHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("received PUT %s\n", r.URL.Path)

	auth, err := r.Cookie("token")
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	u, err := token.GetUser(auth.Value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	switch r.URL.Path {
	case "/editprofile":
		r.ParseMultipartForm(1024)
		name := r.MultipartForm.Value["name"]
		address := r.MultipartForm.Value["address"]
		telephone := r.MultipartForm.Value["telephone"]

		if len(name) > 0 {
			u.Name = name[0]
		}
		if len(address) > 0 {
			u.Address = address[0]
		}
		if len(telephone) > 0 {
			u.Telephone = telephone[0]
		}

		err = user.Update(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	w.WriteHeader(http.StatusNoContent)
}

// PostHandler is used to handle data comming from the website
func PostHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("received POST %s\n", r.URL.Path)

	switch r.URL.Path {
	case "/login":
		auth := r.Header.Get("Authorization")
		_, err := token.GetUser(r.Header.Get("Authorization"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		w.Header().Add("Set-Cookie", "token="+auth)
		w.WriteHeader(http.StatusNoContent)
	case "/signup":
		// Google signup starts with Bearer and ours with Basic
		var u user.User
		err := r.ParseMultipartForm(1024)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		email, ok := r.MultipartForm.Value["email"]
		token, ok2 := r.MultipartForm.Value["token"]
		if !ok {
			http.Error(w, "missing email form data", http.StatusBadRequest)
			return
		}
		if !ok2 {
			http.Error(w, "missing token form data", http.StatusBadRequest)
			return
		}

		u.Email = email[0]
		u.Token = "google"
		if token[0][:5] == "Basic" {
			// Open token and search
			info, err := base64.URLEncoding.DecodeString(token[0][6:])
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			u.Password = strings.Split(string(info), ":")[1]
			u.Token = "internal"
		}

		err = user.Save(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Add("Set-Cookie", "token="+token[0])
		w.WriteHeader(http.StatusCreated)
	case "/signout":
		auth, err := r.Cookie("token")
		if err != nil {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		w.Header().Add("Set-Cookie", "token="+auth.Value+"; expires=Thu, 01-Jan-1970 00:00:01 GMT")
		w.WriteHeader(http.StatusNoContent)
	case "/forgotpass":
		err := r.ParseMultipartForm(1024)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		email, ok := r.MultipartForm.Value["email"]
		if !ok {
			http.Error(w, "missing email form data", http.StatusBadRequest)
			return
		}

		expiration, err := password.SetTemp(email[0])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = password.SendResetLink(email[0], expiration)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
	case "/resetpass":
		err := r.ParseMultipartForm(1024)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		expirationString, ok := r.MultipartForm.Value["id"]
		newPass, ok2 := r.MultipartForm.Value["pass"]
		if !ok {
			http.Error(w, "missing id form data", http.StatusBadRequest)
			return
		}
		if !ok2 {
			http.Error(w, "missing pass form data", http.StatusBadRequest)
			return
		}

		expiration, err := strconv.ParseInt(expirationString[0], 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Check expiration date
		err = password.CheckExpiration(expiration)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		err = password.Reset(expiration, newPass[0])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
	}
}

// GetHandler is used to serve pages appropriately
func GetHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("received GET %s\n", r.URL.Path)

	// Check token
	var u user.User
	switch r.URL.Path {
	case "/profile.html":
		fallthrough
	case "/editprofile.html":
		auth, err := r.Cookie("token")
		if err != nil {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
		log.Printf("cookie: %s\n", auth.Value)

		u, err = token.GetUser(auth.Value)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
	}

	if r.URL.Path == "/" {
		r.URL.Path = "/index.html"
	}
	path := "./static/" + strings.ReplaceAll(r.URL.Path, "..", "")

	// Open page and read
	file, err := os.Open(path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	stats, err := file.Stat()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	content := make([]byte, stats.Size())
	file.Read(content)

	// Templating
	t := template.Must(template.New("profile").Parse(string(content)))

	w.Header().Add("Cache-Control", "no-cache, private, must-revalidate, max-stale=0, post-check=0, pre-check=0 no-store")
	w.Header().Add("Pragma", "no-cache, no-store")
	w.Header().Add("Expires", "Thu, 01 Dec 1994 16:00:00 GMT")

	// Execute the template for each recipient.
	log.Printf("got user: %+v\n", u)
	err = t.Execute(w, u)
	if err != nil {
		log.Println(err.Error())
	}
}
