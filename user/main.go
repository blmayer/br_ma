package user

import (
	"br_ma/mysql"
)

// User type represents a user
type User struct {
	Name      string
	Email     string
	Token     string
	Address   string
	Telephone string
	Password  string
}

// Get returns a user struct for a given token
func Get(email string) (u User) {
	// Make query on mysql
	mysql.DB.QueryRow(`
	SELECT
		email,
		fullName,
		telephone,
		address,
		token
	FROM test.users
	WHERE email = ?`,
		email,
	).Scan(&u.Email, &u.Name, &u.Telephone, &u.Address, &u.Token)

	return
}

// Save adds an user to the database
func Save(u User) error {
	_, err := mysql.DB.Exec(`
	INSERT
	INTO test.users (
		email,
		token,
		password
	) VALUES (
		?,
		?,
		?
	)`, u.Email, u.Token, u.Password,
	)
	return err
}

// Update changes an user in the database
func Update(u User) error {
	_, err := mysql.DB.Exec(`
	UPDATE
		test.users 
	SET
		fullName = ?,
		address = ?,
		telephone = ?
	WHERE email = ?
	`, u.Name, u.Address, u.Telephone, u.Email,
	)
	return err
}
