package main

import (
	"br_ma/handlers"
	"net/http"
	"os"
)

func server(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		handlers.GetHandler(w, r)
	case http.MethodPost:
		handlers.PostHandler(w, r)
	case http.MethodPut:
		handlers.PutHandler(w, r)
	default:
		http.Error(w, "", http.StatusMethodNotAllowed)
	}
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		panic("PORT not found")
	}

	http.HandleFunc("/", server)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		panic(err)
	}
}
